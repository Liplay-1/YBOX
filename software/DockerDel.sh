#!/bin/bash

# Stop and remove all running Docker containers
sudo docker stop $(sudo docker ps -aq)
sudo docker rm $(sudo docker ps -aq)

# Check if Docker process is still running and remove pid file
if ps -ef | grep -v grep | grep docker > /dev/null; then
    sudo killall docker
fi
if [ -f /var/run/docker-ssd.pid ]; then
    sudo rm /var/run/docker-ssd.pid
fi

# Remove Docker installation package
if [ -x "$(command -v yum)" ]; then
    sudo yum remove docker-ce docker-ce-cli containerd.io docker-compose-plugin
elif [ -x "$(command -v apt-get)" ]; then
    sudo apt-get remove docker-ce docker-ce-cli containerd.io docker-compose-plugin
fi

# Remove Docker images, containers, volumes and networks
sudo rm -rf /var/lib/docker
sudo rm -rf /var/lib/containerd

# Remove Docker repository
sudo rm -rf /etc/docker/

# Remove Docker socket files
sudo rm -rf /var/run/docker
sudo rm -rf /var/run/dockershim.sock

echo "Docker has been successfully uninstalled from this machine."