#!/bin/basharia2caria2c
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export LANG=zh_CN.UTF-8
#安装云崽依赖软件
#安装FFmpeg
function install_ffmpeg() {
    if [ -f "/root/FFmpeg/ffmpeg" ]; then
        return 0
    fi
    local FFMPEG_URL="$1"
    echo "FFmpeg 下载地址： $FFMPEG_URL"
    cd ~
    aria2c -s 8 -o "$(basename "$1")" "$1"
    # 解压并移动到 /usr/local/node
    tar xf "/root/$(basename "$1")"  
    #sudo mv "/root/$(basename "$1")"  /usr/local/node
    mkdir -p "/root/FFmpeg"
    cd  "/root/$(basename "$1" .tar.xz)/bin" && cp ffmpeg ffprobe /root/FFmpeg/
    # 下载失败则重新下载
    if ! [ -f "/root/FFmpeg/ffmpeg" ]; then
        echo "FFmpeg 安装失败，等待5秒尝试..."
        sleep 5
        download=1
        # 重新下载（循环）
        while [ $download -lt 5 ]; do
            echo "FFmpeg 下载地址： $FFMPEG_URL"
            cd ~
            aria2c -s 8 -o "$(basename "$1")" "$1"
            tar -xf "/root/$(basename "$1")"
            mkdir -p "/root/FFmpeg"
            cd  "/root/$(basename "$1" .tar.xz)/bin" && cp ffmpeg ffprobe /root/FFmpeg/
            sudo rm -f "/root/$(basename "$1")" 
            # 如果下载成功则跳出循环
            if [ -f "/root/FFmpeg/ffmpeg" ]; then
                download=5
            else
                echo "FFmpeg 安装失败，等待5秒尝试..."
                sleep 5
                download=$[$download+1]
            fi
        done
    fi

}
# 安装 Node.js
function install_nodejs() {
  # 检查是否已经安装 Node.js
    if [ -d "/usr/local/node" ]; then
        return 0
    fi
  # 下载 Node.js tarball
  cd ~
  #curl -O "$1"
  aria2c -s 8 -o "$(basename "$1")" "$1"
  # 解压并移动到 /usr/local/node
  tar xf "/root/$(basename "$1")"  
  #sudo mv "/root/$(basename "$1")"  /usr/local/node
  sudo mv "/root/$(basename "$1" .tar.xz)" /usr/local/node
  # 添加 Node.js 到 PATH
    echo "# Node.js" >> $HOME/.bashrc
    echo "export PATH=/usr/local/node/bin:\$PATH" >> $HOME/.bashrc
    export PATH=/usr/local/node/bin:$PATH
    # 下载失败则重新下载
    if ! [ -d "/usr/local/node" ]; then
        echo "Node.js 安装失败，等待5秒尝试..."
        sleep 5
        download=1
        # 重新下载（循环）
        while [ $download -lt 5 ]; do
            #curl -O "$1"
            aria2c -s 8 -o "$(basename "$1")" "$1"
            # 解压并移动到 /usr/local/node
            tar xf "/root/$(basename "$1")"  
            #sudo mv "/root/$(basename "$1")"  /usr/local/node
            sudo mv "/root/$(basename "$1" .tar.xz)" /usr/local/node
            sudo rm -f "/root/$(basename "$1")"
            export PATH=/usr/local/node/bin:$PATH
            # 如果下载成功则跳出循环
            if [ -d "/usr/local/node" ]; then
                download=5
            else
                echo "Node.js 安装失败，等待5秒尝试..."
                sleep 5
                download=$[$download+1]
            fi
        done
    fi


}

#定义函数用于判断是否已经安装成功
function check_install() {
    if ! [ -f "/root/FFmpeg/ffmpeg" ]; then
        echo "FFmpeg 安装失败"
    else
        echo "FFmpeg 安装完成"
        rm -f /root/ffmpeg.tar.xz
        rm -rf /root/ffmpeg*-static
    fi
    if ! [ -d "/usr/local/node" ]; then
        echo "Node.js 安装失败,安装停止..."
        exit 1
    else
        echo "Node.js 安装完成"
        rm -f /root/node-v*
    fi
}


function install() {
    local ARCH=$(uname -m)
    local FFMPEG_URL=""
    if [ "$ARCH" == "x86_64" ]; then
        install_ffmpeg "https://ghproxy.com/https://github.com/BtbN/FFmpeg-Builds/releases/download/latest/ffmpeg-master-latest-linux64-gpl-shared.tar.xz"
        install_nodejs "https://cdn.npmmirror.com/binaries/node/v18.16.0/node-v18.16.0-linux-x64.tar.xz"
    elif [ "$ARCH" == "aarch64" ]; then
        install_ffmpeg "https://ghproxy.com/https://github.com/BtbN/FFmpeg-Builds/releases/download/latest/ffmpeg-master-latest-linuxarm64-gpl-shared.tar.xz"
        install_nodejs "https://cdn.npmmirror.com/binaries/node/v16.14.0/node-v18.16.0-linux-arm64.tar.xz"
    else
        echo "不支持该架构: $ARCH"
        return 1
    fi
    check_install
}
install
