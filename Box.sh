#!/bin/bash
echo "正在获取Go_Box..."
sed -i '/alias box="bash \/root\/box.sh"/d' ~/.bashrc
ARCH=$(uname -m)
if [ "$ARCH" = "x86_64" ]; then
    wget  http://222.186.59.77:30012/Go_Box-x86.tar.xz
    tar -xf Go_Box-x86.tar.xz
    rm -rf Go_Box-x86.tar.xz
elif [ "$ARCH" = "aarch64" ]; then
    wget http://222.186.59.77:30012/Go_Box-arm.tar.xz
    tar -xf Go_Box-arm.tar.xz
    rm -rf Go_Box-arm.tar.xz
else
    echo "无法确定系统架构"
    exit
fi

echo "alias box='/root/Go_Box/Go_Box'" >> ~/.bashrc
bash