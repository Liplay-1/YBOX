#!/bin/bash
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export LANG=zh_CN.UTF-8
#定义函数，用于判读Ubuntu系统架构
function main() {
    ARCH=$(uname -m)
    if [ "$ARCH" = "x86_64" ]; then
        echo "系统为x86架构正在执行对应的脚本..."
        #配置云崽运行环境
        #判断系统发行版
        if [ -f /etc/redhat-release ]; then
            echo "系统为Centos"
            curl -sL https://gitee.com/Liplay-1/YBOX/raw/master/system/Centos.sh > /root/Centos.sh && bash /root/Centos.sh
        elif [ -f /etc/lsb-release ]; then
            echo "系统为Ubuntu"
            curl -sL https://gitee.com/Liplay-1/YBOX/raw/master/system/Ubuntu.sh > /root/Ubuntu.sh && bash /root/Ubuntu.sh
        elif [ -f /etc/debian_version ]; then
            echo "系统为Debian"
            curl -sL https://gitee.com/Liplay-1/YBOX/raw/master/system/Debian.sh > /root/Debian.sh && bash /root/Debian.sh
        else
            echo "无法确定系统发行版..."
            exit
        fi


    elif [ "$ARCH" = "aarch64" ]; then
        echo "系统为arm架构正在执行对应的脚本..."

        if cat /etc/os-release | grep VERSION_ID=\"20.04\" >/dev/null 2>&1; then
            echo "系统为Ubuntu20.04"
            curl -sL https://gitee.com/Liplay-1/YBOX/raw/master/system/Ubuntu_20.04_arm.sh > /root/Ubuntu_20.04_arm.sh && bash /root/Ubuntu_20.04_arm.sh
        else
            echo "不支持当前系统版本..."
            exit
        fi
        # 在这里执行针对 aarch64 架构的操作
    else
        echo "无法确定系统架构..."
        exit
    fi
}
main