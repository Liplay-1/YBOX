#!/bin/bash
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export LANG=zh_CN.UTF-8
#安装系统依赖项
echo "执行一键安装脚本,免配置"
echo "开始配置云崽运行环境..."
echo "如果脚本在运行过程中报错，可以先加群提问：546537258"
# 备份原有的repo文件
cp /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.bak

# 下载清华大学开源软件镜像站的repo文件
if [[ $(cat /etc/centos-release | awk '{print $4}') == 7* ]]; then
  sudo sed -e 's|^mirrorlist=|#mirrorlist=|g' \
         -e 's|^#baseurl=http://mirror.centos.org/centos|baseurl=https://mirrors.tuna.tsinghua.edu.cn/centos|g' \
         -i.bak \
         /etc/yum.repos.d/CentOS-*.repo
    #解决gcc
    if [! -f /lib64/libstdc++.so.6.0.26 ]; then 
        rm -rf /lib64/libstdc++.so.6
        curl -o /lib64/libstdc++.so.6.0.26 http://222.186.59.77:30012/libstdc-so/libstdc++.so.6.0.26
        chmod 755 /lib64/libstdc++.so.6.0.26
        ln -s /lib64/libstdc++.so.6.0.26 /lib64/libstdc++.so.6
    fi
elif [[ $(cat /etc/centos-release | awk '{print $4}') == 8* ]]; then
    mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup
    curl -o /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-vault-8.5.2111.repo
    # epel
    yum install -y https://mirrors.aliyun.com/epel/epel-release-latest-8.noarch.rpm
else
  echo "源尚未配置..."
fi

# 清除yum缓存并生成新的yum缓存
yum install epel-release -y
yum clean all
yum repolist
yum makecache
yum install aria2 -y
yum install redis -y
systemctl start redis 
systemctl enable redis
yum install git -y
yum install libdrm* -y
yum install libgbm* -y
yum install at-spi2-atk -y
yum groupinstall fonts -y
yum install gcc -y
yum install cmake -y
aria2c -s 12 https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
yum install ./google-chrome-stable_current_x86_64.rpm -y
yum install pango.x86_64 libXcomposite.x86_64 libXcursor.x86_64 libXdamage.x86_64 libXext.x86_64 libXi.x86_64 libXtst.x86_64 cups-libs.x86_64 libXScrnSaver.x86_64 libXrandr.x86_64 GConf2.x86_64 alsa-lib.x86_64 atk.x86_64 gtk3.x86_64 -y
curl -sL https://gitee.com/Liplay-1/YBOX/raw/master/software/Environment.sh > /root/Environment.sh && bash /root/Environment.sh
echo "基础运行环境配置完成，等待2s自动执行云崽安装脚本..."
sleep 2
curl -sL https://gitee.com/Liplay-1/YBOX/raw/master/Go_Box/Scripts/install.sh > /root/install.sh && bash /root/install.sh