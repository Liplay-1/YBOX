#!/bin/bash
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export LANG=zh_CN.UTF-8
#安装系统依赖项
echo "执行一键安装脚本,免配置"
echo "开始配置云崽运行环境..."
echo "如果脚本在运行过程中报错，可以先加群提问：546537258"
apt update
apt install sudo
sudo apt install git -y
sudo apt install vim -y
sudo apt install redis -y
apt-get install lolcat -y
sudo apt install aria2 -y
sudo apt install gcc -y
sudo apt install cmake -y
sudo apt install curl -y
# 安装 Chromium 浏览器的依赖
sudo apt install -y gconf-service libasound2 libgbm1 xdg-utils libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates fonts-liberation
redis-server --save 900 1 --save 300 10 --daemonize yes
#字体
apt install -y --force-yes --no-install-recommends fonts-wqy-microhei
#依赖
# 下载并安装适用于 ARM 架构的 Chromium 浏览器
sudo apt-get install -y chromium-browser
#安装node和FFpeg
curl -sL https://gitee.com/Liplay-1/YBOX/raw/master/software/Environment.sh > /root/Environment.sh && bash /root/Environment.sh
echo "基础运行环境配置完成，等待2s自动执行云崽安装脚本..."
sleep 2
curl -sL https://gitee.com/Liplay-1/YBOX/raw/master/Go_Box/Scripts/install.sh > /root/install.sh && bash /root/install.sh