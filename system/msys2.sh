#!/bin/bash
export MSYS=winsymlinks:nativestrict
#msys2配置清华源，就配置这个就行
cp -r /etc/pacman.d /etc/pacman.d.bak
sed -i "s#https\?://mirror.msys2.org/#https://mirrors.tuna.tsinghua.edu.cn/msys2/#g" /etc/pacman.d/mirrorlist*
pacman -Syu --noconfirm
#安装常用软件
ln -s "/c/Program Files/Google/Chrome/Application/chrome.exe" "/usr/local/bin/chromium"
pacman -S nodejs-lts-fermium git vim zsh curl wget redis git lolcat axel --noconfirm
pacman -S --force --noconfirm --needed mingw-w64-x86_64-fonts-wqy-zenhei
