#!/bin/bash
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export LANG=zh_CN.UTF-8
cd /root
sed -i '/alias box="bash \/root\/box.sh"/d' ~/.bashrc
clear
echo "正在执行执行配置脚本..."
redis=127.0.0.1
if [ -d /root/Go_Box/bak_YunZai ]; then
    #询问是否恢复备份
    read -p "检测到备份文件夹，是否恢复数据（y/n）:" bak
    if [ $bak = "y" ]; then
        echo "正在恢复数据..."
        cp -r /root/Go_Box/bak_YunZai/data /root/Yunzai-Bot
        cp -r /root/Go_Box/bak_YunZai/config /root/Yunzai-Bot/config
    else
        echo "正在删除备份文件夹..."
        rm -rf /root/Go_Box/bak_YunZai
        read -p "请输入QQ小号（机器人）:" QQ
        read -p "请输入QQ小号密码 （机器人）:" key
        read -p "请输入主人QQ（大号）:" QQmaster
        if [ ! -d /root/default_config/ ]; then
            cp -r  /root/Yunzai-Bot/config/default_config/ /root/
            sed -r -i "s/(host:).*/\1 $redis/" /root/default_config/redis.yaml
            sed -r -i "s/(qq:).*/\1 $QQ/" /root/default_config/qq.yaml
            sed -r -i "s/(pwd:).*/\1 '$key'/" /root/default_config/qq.yaml
            #修改接口
            sed -r -i "s/(platform:).*/\1 '6'/" /root/default_config/qq.yaml
            #qq
            sed -i "7i \ \-\ $QQmaster" /root/default_config/other.yaml
            sed -i "s#ffmpeg_path: #ffmpeg_path: /root/FFmpeg/ffmpeg#g" /root/default_config/bot.yaml
            sed -i "s#ffprobe_path: #ffprobe_path: /root/FFmpeg/ffprobe#g" /root/default_config/bot.yaml 
        fi
            
    fi
else
    read -p "请输入QQ小号（机器人）:" QQ
    read -p "请输入QQ小号密码 （机器人）:" key
    read -p "请输入主人QQ（大号）:" QQmaster
    if [ ! -d /root/default_config/ ]; then
        cp -r  /root/Yunzai-Bot/config/default_config/ /root/
        sed -r -i "s/(host:).*/\1 $redis/" /root/default_config/redis.yaml
        sed -r -i "s/(qq:).*/\1 $QQ/" /root/default_config/qq.yaml
        sed -r -i "s/(pwd:).*/\1 '$key'/" /root/default_config/qq.yaml
        #修改接口
        sed -r -i "s/(platform:).*/\1 '6'/" /root/default_config/qq.yaml
        #qq
        sed -i "7i \ \-\ $QQmaster" /root/default_config/other.yaml
        sed -i "s#ffmpeg_path: #ffmpeg_path: /root/FFmpeg/ffmpeg#g" /root/default_config/bot.yaml
        sed -i "s#ffprobe_path: #ffprobe_path: /root/FFmpeg/ffprobe#g" /root/default_config/bot.yaml 
    fi
fi
if [ -d /root/default_config/ ]; then
    cp /root/default_config/* /root/Yunzai-Bot/config/config/
    rm -rf /root/default_config/
fi


read -p "输入锅巴自定义地址(如果服务器部署或仅安装云崽直接回车跳过设置):" GuoBa
#锅巴地址
if [ $GuoBa ]; then
    echo "当前值存在，正在修改锅巴模板文件：$GuoBa"
    sed -r -i "s/(splicePort:).*/\1 false/" /root/Yunzai-Bot/plugins/Guoba-Plugin/defSet/application.yaml
    sed -r -i "s/(host:).*/\1 $GuoBa/" /root/Yunzai-Bot/plugins/Guoba-Plugin/defSet/application.yaml
fi



ARCH=$(uname -m)
if [ "$ARCH" = "x86_64" ]; then
    aria2c  http://222.186.59.77:30012/Go_Box-x86.tar.xz
    tar -xf Go_Box-x86.tar.xz
    rm -rf Go_Box-x86.tar.xz
elif [ "$ARCH" = "aarch64" ]; then
    aria2c http://222.186.59.77:30012/Go_Box-arm.tar.xz
    tar -xf Go_Box-arm.tar.xz
    rm -rf Go_Box-arm.tar.xz
else
    echo "无法确定系统架构"
    exit
fi

echo "alias box='/root/Go_Box/Go_Box'" >> ~/.bashrc

#安装完成
echo -e "\033[31m安装完成,请输入:box 输入1 完成机器人登录操作\033[0m"
echo -e "\033[31m脚本开源免费，禁止倒卖，仅供交流学习使用，严禁用于任何商业用途和非法行为\033[0m"
echo "若出现token过期、网络不稳定、安全提醒、ticket输入错误或者已失效等等错误"
echo "可能token提取不完整，建议使用谷歌、Edge浏览器提取token或者使用滑动app自动获取，如果还是出现这种报错可以尝试更换登录接口'box 选8'"
echo "滑动验证app下载地址：https://wwp.lanzouy.com/i6w3J08um92h 密码:3kuu"
echo "如果还是不行，那就是账户问题了，建议跟换一个机器人QQ"
echo "还有其他问题欢迎加入QQ群：546537258"


#删除安装包
rm -rf /root/conf.sh*
rm -rf /root/google-chrome-*
rm -rf /root/install.sh*
rm -rf /root/node-v*
rm  -rf /root/Enviro*
rm -rf /root/Ubun*
rm -rf /root/Cento*
rm -rf /root/Debi*
rm -rf /root/main.sh*
rm -rf /root/ffmpeg-*
bash

