#!/bin/bash
export PATH=$PATH:/usr/local/bin/node
# 定义颜色常量
color_blue_start='\033[34m'
color_end='\033[0m'


# 修复函数
function repair() {
    clear
    echo -e " $color_blue_start
	==========常见问题修复===========
	  1.更新ICQQ最新
	  2.更换Gitee拉取更新 
	  3.修复pnpm start 
	  4.修改锅巴自定义地址（挂机宝搭建）
	  5.修复登录235错误
	  6.修改锅巴端口(服务器搭建)
	  0.主菜单"
    read -p "	请输入菜单编号：:" run_function
    case $run_function in
        1)
            pnpm config set registry https://registry.npmmirror.com 
            cd $yunzai_path
            pnpm install 
            pnpm uninstall icqq
            pnpm install icqq -w
            repair
            ;;
        2)
            cd $yunzai_path
            git remote set-url origin https://gitee.com/yoimiya-kokomi/Miao-Yunzai.git
            git fetch
            git stash
            git checkout main 
            git pull
            ;;
        3)
            pnpm install pm2 -w
            pnpm install
            repair
            ;;
        4)
            if [ -f $yunzai_path/plugins/Guoba-Plugin/config/application.yaml ]; then
                read -p "输入锅巴自定义地址:" GuoBa
                # 锅巴地址
                if [ $GuoBa ]; then
                sed -r -i "s/(splicePort:).*/\1 false/" $yunzai_path/plugins/Guoba-Plugin/config/application.yaml
                sed -r -i "s/(host:).*/\1 $GuoBa/" $yunzai_path/plugins/Guoba-Plugin/config/application.yaml
                repair
                fi
            else
                echo -e "$message_error_tag 无法修改！没有初始化或者没有安装锅巴插件"
                sleep 3
                clear
                repair               
            fi
            ;;
        5)
            rm -rf  $yunzai_path/data/icqq*
            echo "修复完成，若出现登录错误45，输入box把接口改成旧安卓"
            sleep 3
            repair
            ;;
        6)
            if [ -f $yunzai_path/plugins/Guoba-Plugin/config/application.yaml ]; then
                read -p "输入锅巴端口:" GuoBa
                # 锅巴地址
                if [ $GuoBa ]; then
                    sed -r -i "s/(port:).*/\1 $GuoBa/" $yunzai_path/plugins/Guoba-Plugin/config/application.yaml
                    echo "修改完成，可能需要放行防火墙才能正常访问，挂机宝搭建，不懂不要修改"
                    sleep 3
                    repair
                fi
            else
                echo -e "$message_error_tag 无法修改！没有初始化或者没有安装锅巴插件"
                sleep 3
                clear
                repair               
            fi
            ;;
        0)
            # 返回主菜单
            echo "返回主菜单"
            exit 0
            ;;
        *)
            echo "无效的输入，请重新输入"
            sleep 1
            clear
            repair
            ;;
    esac
}

# 从Go语言中传递变量给Shell脚本
yunzai_path=$1

# 调用repair函数
repair
