#/bin/bash
yunzai_path="/root/Yunzai-Bot"

if [ ! -d /root/GoBox ]; then 
    echo "GoBox未安装"
    exit 1
fi 

echo "发现新版本，开始升级..."
echo "v2版本目录架构发生改变,云崽或喵崽，将移动到/root/GoBox目录下...."
echo "升级中..."
echo "按Ctlr+C退出升级.."
sleep 10
rm -rf /root/GoBox
# 删除别名
sed -i '/alias box/d' ~/.bashrc
curl -sL https://gitee.com/Liplay-1/YBOX_2/raw/master/GoBox/GoBox_install.sh > /root/install.sh && bash /root/install.sh
# 判断存在gobox目录
if [ ! -d /root/GoBox ]; then 
    echo "升级失败，请手动升级..."
else
    echo "升级成功"
    rm -rf /root/install.sh
    mv /root/Yunzai-Bot /root/GoBox/Yunzai-Bot
    sed -r -i "s/(Bot_id:).*/\1 3/"  /root/GoBox/config.yaml
    if grep -q "YunZaiBotPath" /root/GoBox/config.yaml; then
        echo "配置项已存在..."
    else
        # 将配置项写入文件
        echo "YunZaiBotPath: \"./Yunzai-Bot\"" >> /root/GoBox/config.yaml
        echo "配置项已写入..."
    fi
fi
