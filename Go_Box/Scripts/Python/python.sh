#!/bin/bash
#定义shell颜色变量
color_blue_start="\033[34m"
function python_manager {
    #判断发行版是不是UBuntu
    if [ ! -f /etc/lsb-release ]; then
        echo "不是Ubuntu发行版,请执行发行版的脚本"
        sleep 2
        exit 0
    fi
    clear
    echo -e " $color_blue_start
	=========Python管理器==========
	  1.安装Python3.9
	  2.卸载Python3.9 
	  3.pip换国内加速源 
	  4.设置Python/PiP为控制台(bash)默认
	  0.主菜单"
    while true; do
        read -p "	请输入菜单编号：" run_function
        case $run_function in
            0) main_menu;;
            1) Python_install; break;;
            2) Python_uninstall; break;;
            3) pip_source; break;;
            4) Python_default; break;;
            *) echo "	请输入有效的数字！";;
        esac
    done
}
function main_menu {
    clear
    exit 0
}
function Python_install {
    clear
    #判断是否安装Python3.9,并且需要用户确认安装
    if [ ! -f /usr/local/bin/python3.9 ]; then
        echo "Python3.9没有安装"
        echo -e "\n 1.安装 \n 2.取消 \n 0.返回 \n"
        read -p "请输入你的选择（输入数字）:" Python_install_id
        if [[ "${Python_install_id}" == "1" ]]; then
            apt install -y build-essential libreadline-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev libffi-dev zlib1g-dev zlib1g
            cd /tmp
            wget -O Python-3.9.2rc1.tar.xz https://repo.huaweicloud.com/python/3.9.2/Python-3.9.2rc1.tar.xz
            tar -xvf Python-3.9.2rc1.tar.xz
            cd Python-3.9.2rc1
            ./configure --enable-optimizations
            make 
            make altinstall
            echo "安装完成"
        elif [[ "${Python_install_id}" == "2" ]]; then
            echo "用户已取消执行"
            sleep 2
        elif [[ "${Python_install_id}" == "0" ]]; then
            python_manager
        else
            Python_install
        fi
    else
        echo "Python3.9已经安装"
        sleep 2
        python_manager
    fi


}

#Python 卸载
function Python_uninstall {
    clear
    #判断是否安装Python,并且需要用户确认卸载
    if [ -f /usr/local/bin/python3.9 ]; then
        echo "Python3.9已经安装"
        echo -e "\n 1.卸载 \n 2.取消 \n 0.返回 \n"
        read -p "请输入你的选择（输入数字）:" Python_uninstall_id
        if [[ "${Python_uninstall_id}" == "1" ]]; then
            rm -rf  /tmp/Python-3.9.2rc1
            rm -rf /usr/local/bin/python3.9
            rm -rf /usr/local/bin/pip3.9
            rm -rf /usr/local/lib/python3.9
            rm -rf /usr/local/lib/python3.9/site-packages
            rm -rf /usr/local/include/python3.9
        elif [[ "${Python_uninstall_id}" == "2" ]]; then
            echo "用户已取消执行"
            sleep 2
            python_manager
        elif [[ "${Python_uninstall_id}" == "0" ]]; then
            python_manager
        else
            Python_uninstall
        fi
    else
        echo "Python3.9没有安装"
        sleep 2
        python_manager
    fi

    

}

#pip换国内加速源，清华、阿里、华为 ，需要判断是否安装了Python3.9
function pip_source {
    clear
    #判断是否安装Python,并且需要用户确认卸载
    if [ -f /usr/local/bin/python3.9 ]; then
        echo "Python3.9已经安装"
        echo -e "\n 1.清华 \n 2.阿里 \n 3.华为 \n 0.返回 \n"
        read -p "请输入你的选择（输入数字）:" pip_source_id
        if [[ "${pip_source_id}" == "1" ]]; then
            pip3.9 config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple
        elif [[ "${pip_source_id}" == "2" ]]; then
            pip3.9 config set global.index-url https://mirrors.aliyun.com/pypi/simple/
        elif [[ "${pip_source_id}" == "3" ]]; then
            pip3.9 config set global.index-url https://mirrors.huaweicloud.com/repository/pypi/simple
        elif [[ "${pip_source_id}" == "0" ]]; then
            python_manager
        else
            pip_source
        fi
    else
        echo "Python3.9没有安装"
        sleep 2
        python_manager
    fi    
    

}


#设置Python pip为控制台（CMD,shell）默认，需要判断是否安装了Python3.9
function Python_default {
    clear
    if [ -f /usr/local/bin/python3.9 ]; then
        echo "Python3.9已经安装"
        echo -e "\n 1.设置控制台默认 \n 2.取消设置控制台默认 \n 0.返回 \n"
        read -p "请输入你的选择（输入数字）:" Python_default_id
        if [[ "${Python_default_id}" == "1" ]]; then
            update-alternatives --install /usr/bin/python python /usr/local/bin/python3.9 1
            update-alternatives --install /usr/bin/pip pip /usr/local/bin/pip3.9 1
        elif [[ "${Python_default_id}" == "2" ]]; then
            update-alternatives --remove python /usr/local/bin/python3.9
            update-alternatives --remove pip /usr/local/bin/pip3.9
        elif [[ "${Python_default_id}" == "0" ]]; then
            python_manager
        else
            Python_default
        fi
    else
        echo "Python3.9没有安装"
        sleep 2
        python_manager
    fi    
    

}
python_manager