#!/bin/bash
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export LANG=zh_CN.UTF-8
export PATH=/usr/local/node/bin:$PATH
color_blue_start="\033[34m"
#备份文件
function backup_file() {
    # 判断文件夹是否存在
    if [ -d $yunzai_path ]; then
        #判断是否有备份文件夹，有则备份，没有则创建
        if [ ! -d "/root/Go_Box/bak_YunZai" ]; then
            echo "正在备份文件..."
            mkdir   /root/Go_Box/bak_YunZai
            cp -r  $yunzai_path/data /root/Go_Box/bak_YunZai
            cp -r  $yunzai_path/config/config   /root/Go_Box/bak_YunZai
            echo "删除云崽目录.."
            rm -rf $yunzai_path
        else
            echo "正在备份文件..."
            cp -r  $yunzai_path/data /root/Go_Box/bak_YunZai
            cp -r  $yunzai_path/config/config   /root/Go_Box/bak_YunZai    
            echo "删除云崽目录.."
            rm -rf $yunzai_path    
        fi
    fi
}
function clone_repo() {
    echo "Cloning repository from $REPO_URL..."
    git clone --depth=1 "$REPO_URL" $yunzai_path
}

# 安装依赖
function install_dependencies() {
    cd $yunzai_path
    echo "Installing dependencies..."
    if [ ! -d "node-mudules/" ]; then
        npm install pnpm -g --registry=https://registry.npmmirror.com
        npm install cnpm -g --registry=https://registry.npmmirror.com
        npm config set registry https://registry.npmmirror.com
        pnpm config set registry https://registry.npmmirror.com
        pnpm install -P
    else
        echo "Dependencies have been installed"
    fi
}

# 安装插件
function install_plugin() {
    cd $yunzai_path
    local plugin_name="$1"
    local plugin_url="$2"
    local plugin_dependencies="$3"
    echo "Installing $plugin_name plugin..."
    if [ ! -d "plugins/$plugin_name" ]; then
        git clone "$plugin_url" "./plugins/$plugin_name"
        #判定是否有依赖
        if [ -n "$plugin_dependencies" ]; then
            cd $yunzai_path
            plugin_dependencies=${plugin_dependencies}" --force"
            $plugin_dependencies
        fi
    else
        echo "$plugin_name plugin has been installed"
    fi
}
#插件列表
function plugin_index(){
    install_plugin "Guoba-Plugin" "https://gitee.com/guoba-yunzai/guoba-plugin.git" "pnpm install --filter=guoba-plugin" #锅巴     
    install_plugin "xiaoyao-cvs-plugin" "https://gitee.com/Ctrlcvs/xiaoyao-cvs-plugin.git" "" #逍遥
    install_plugin "xianxin-plugin" "https://gitee.com/xianxincoder/xianxin-plugin.git" "" #闲心
    install_plugin "earth-k-plugin" "https://gitee.com/SmallK111407/earth-k-plugin.git" "pnpm add https-proxy-agent -w"  #土块
    install_plugin "lin-plugin" "https://gitee.com/go-farther-and-farther/lin-plugin.git" "" #林
}

# 主菜单
function main_menu() {
    clear
    echo -e " $color_blue_start
	==========欢迎使用一键安装===========
	  1.仅安装喵崽（不包含插件）
	  2.安装喵崽和常用插件
	  3.仅安装云崽（不包含插件）
	  4.安装云崽和常用插件
	  0.主菜单/退出"
    read -p "	请输入你的选择（输入数字）:" run_function
    case $run_function in
        1)
            REPO_URL="https://gitee.com/yoimiya-kokomi/Miao-Yunzai.git"
            backup_file #备份文件
            clone_repo #克隆仓库
            install_dependencies #安装依赖
            install_plugin "miao-plugin" "https://gitee.com/yoimiya-kokomi/miao-plugin.git" "pnpm i" #喵喵
            curl -sL https://gitee.com/Liplay-1/YBOX/raw/master/conf.sh > /root/conf.sh && bash /root/conf.sh
            ;;
        2)
            REPO_URL="https://gitee.com/yoimiya-kokomi/Miao-Yunzai.git"
            backup_file #备份文件
            clone_repo #克隆仓库
            install_dependencies #安装依赖
            install_plugin "miao-plugin" "https://gitee.com/yoimiya-kokomi/miao-plugin.git" "pnpm i" #喵喵
            plugin_index #插件列表
            curl -sL https://gitee.com/Liplay-1/YBOX/raw/master/conf.sh > /root/conf.sh && bash /root/conf.sh
     
            ;; 
        3)
            REPO_URL="https://gitee.com/yoimiya-kokomi/Yunzai-Bot.git"
            backup_file
            clone_repo
            install_dependencies 
            install_plugin "miao-plugin" "https://gitee.com/yoimiya-kokomi/miao-plugin.git" "pnpm i" #喵喵
            curl -sL https://gitee.com/Liplay-1/YBOX/raw/master/conf.sh > /root/conf.sh && bash /root/conf.sh
            ;;
        4)
            REPO_URL="https://gitee.com/yoimiya-kokomi/Yunzai-Bot.git"
            backup_file
            clone_repo
            install_dependencies 
            install_plugin "miao-plugin" "https://gitee.com/yoimiya-kokomi/miao-plugin.git" "pnpm i" #喵喵
            plugin_index
            curl -sL https://gitee.com/Liplay-1/YBOX/raw/master/conf.sh > /root/conf.sh && bash /root/conf.sh
            ;;
        0)
            exit 0
            ;;
        *)  
            main_menu
            ;;
    esac
}
yunzai_path="/root/Yunzai-Bot"
cd
main_menu