package YunZai

/*
处理云崽相关
初始化
修改QQ登录和登录接口

*/

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"runtime"
	"time"

	"gopkg.in/yaml.v2"
)

type Admin struct {
	MasterQQ        []int64  `yaml:"masterQQ"`
	DisableGuildMsg bool     `yaml:"disableGuildMsg"`
	DisablePrivate  bool     `yaml:"disablePrivate"`
	DisableMsg      string   `yaml:"disableMsg"`
	DisableAdopt    []string `yaml:"disableAdopt"`
	WhiteGroup      []string `yaml:"whiteGroup"`
	BlackGroup      []string `yaml:"blackGroup"`
	BlackQQ         []string `yaml:"blackQQ"`
}

// var admin_path string = "config.yaml"
func YunZai_init(YunZaiPath, nodePath string) error {
	//配置Node环境变量
	os.Setenv("NODE_HOME", nodePath)
	// 切换到指定目录
	if err := os.Chdir(YunZaiPath); err != nil {
		return err
	}

	// 启动应用程序
	cmd := exec.Command("bash", "-c", "node app")
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Start(); err != nil {
		return err
	}
	if err := cmd.Wait(); err != nil {
		return err
	}
	return nil
}

func Set_Account(account_path string) {
	var (
		Login_type string
		qq         string
		password   string
	)
	type Account struct {
		QQ         string `yaml:"qq"`
		Password   string `yaml:"pwd"`
		Login_type string `yaml:"platform"`
	}
	var account Account
	date, err := ioutil.ReadFile(account_path)
	if err != nil {
		log.Println("检查文件失败，请初始化云崽/喵崽")
		//睡眠2秒
		time.Sleep(2 * time.Second)
		return
	}
	if err := yaml.Unmarshal(date, &account); err != nil {
		log.Println("解析文件失败,请检查配置文件是否正确")
		time.Sleep(2 * time.Second)
		return
	}
	//提示用户输入QQ号码
	fmt.Printf("请输入QQ号码: ")
	fmt.Scanln(&qq)
	//提示用户输入密码
	fmt.Printf("请输入密码：")
	fmt.Scanln(&password)
	fmt.Printf("登录接口 (1:安卓手机/2:aPad/3:安卓手表/4:MacOS/5:iPad/6:安卓8.8.88)：")
	// fmt.Scanln(&Login_type)
	//做校验只允许输入1-6
	for {
		fmt.Scanln(&Login_type)
		if Login_type == "1" || Login_type == "2" || Login_type == "3" || Login_type == "4" || Login_type == "5" || Login_type == "6" {
			break
		}
		fmt.Printf("登录接口输入错误，请重新输入：")
	}

	//将用户输入的QQ号码和密码写入到结构体中
	account.QQ = qq
	account.Password = password
	account.Login_type = Login_type

	//将结构体转换成yaml格式
	data, err := yaml.Marshal(&account)
	if err != nil {
		log.Println("转换yaml格式失败")
		time.Sleep(2 * time.Second)
		return
	}
	//将yaml格式的数据写入到文件中
	if err := ioutil.WriteFile(account_path, data, 0644); err != nil {
		log.Println("写入文件失败")
		time.Sleep(2 * time.Second)
		return
	}

	//绿色字体输出
	fmt.Println("\033[32m Success：配置文件已写入修改成功,登录选择1,完成初始化\033[0m")
	//查看文件路径
	fmt.Println("文件路径：", account_path)
	time.Sleep(2 * time.Second)
}

// 管理员菜单
func Set_YunZaiAdmin(admin_path string) {
	//判断是否存在admin.yaml文件
	if _, err := os.Stat(admin_path); os.IsNotExist(err) {
		//文件不存在，返回
		log.Println("检查文件失败，请初始化云崽/喵崽")
		time.Sleep(2 * time.Second)
		return

	}
	//获取admin.yaml文件路径
	//菜单
	var set_admin_menu = `
	==========管理员配置==========
	  1.添加管理员
	  2.删除管理员
	  3.查看管理员
	  0.返回上一级
	请输入菜单编号：`
	var Login_type int
	for {
		ClearLog()
		fmt.Printf("\033[34m")
		fmt.Printf(set_admin_menu)
		_, err := fmt.Scanln(&Login_type)
		if err != nil {
			ClearLog()
			fmt.Println("输入错误，请重新输入!")
			//睡眠2秒
			time.Sleep(1 * time.Second)
			continue
		}
		switch Login_type {
		case 1:
			ClearLog()
			fmt.Println("您选择了 1. 添加管理员")
			Set_YunZaiAdmin_Add_manager(admin_path)
		case 2:
			ClearLog()
			fmt.Println("您选择了 2. 删除管理员")
			Del_manager(admin_path)
		case 3:
			ClearLog()
			fmt.Println("您选择了 3. 查看管理员")
			Show_manager(admin_path)
		case 0:
			ClearLog()
			fmt.Println("您选择了 0. 返回上一级")
			return
		default:
			ClearLog()
			fmt.Println("输入错误，请重新输入!")

		}
	}

}

// 添加管理员
func Set_YunZaiAdmin_Add_manager(admin_path string) {
	//读取yaml文件，把用户输入的值添加masterQQ序列
	//读取yaml文件
	var admin Admin
	yamladmin, err := ioutil.ReadFile(admin_path)
	if err != nil {
		log.Println("检查文件失败，请初始化云崽/喵崽")
		time.Sleep(1 * time.Second)
		return
	}
	//解析yaml文件
	err = yaml.Unmarshal(yamladmin, &admin)
	if err != nil {
		log.Println("解析文件失败,请检查配置文件是否正确")
		time.Sleep(1 * time.Second)
		return
	}
	//提示用户输入QQ号码
	var masterQQ int64
	fmt.Printf("请输入QQ号码: ")
	fmt.Scanln(&masterQQ)
	//判断用户输入的QQ号码是否已经存在
	for _, v := range admin.MasterQQ {
		if v == masterQQ {
			//警告黄色字体输出
			fmt.Println("\033[33m Warning：该QQ号码已经存在\033[0m")
			time.Sleep(2 * time.Second)
			return
		}
	}
	// //添加元素到切片中
	//打印文件masterQQ（测试）
	// fmt.Println(admin.MasterQQ)
	admin.MasterQQ = append(admin.MasterQQ, masterQQ)
	//打印切片
	// fmt.Println(admin.MasterQQ)
	//将结构体转换成yaml格式
	// //将结构体转换成yaml格式
	data, err := yaml.Marshal(&admin)
	if err != nil {
		log.Println("转换yaml格式失败")
		time.Sleep(1 * time.Second)
		return
	}
	//打印yaml格式（测试）
	// fmt.Println(string(data))
	//读取源文件，修改masterQQ序列
	//将yaml格式的数据写入到文件中
	//保留源文件的注释
	if err := ioutil.WriteFile(admin_path, data, 0644); err != nil {
		log.Println("写入文件失败")
		time.Sleep(1 * time.Second)
		return
	}

	//绿色字体输出
	fmt.Println("\033[32m Success：配置文件已写入修改成功\033[0m")
	//查看文件路径
	fmt.Println("文件路径：", admin_path)
	time.Sleep(1 * time.Second)
}

// 删除管理员
func Del_manager(admin_path string) {
	//读取yaml文件，把用户输入的值删除masterQQ序列
	//读取yaml文件
	var admin Admin
	yamladmin, err := ioutil.ReadFile(admin_path)
	if err != nil {
		log.Println("检查文件失败，请初始化云崽/喵崽")
		time.Sleep(1 * time.Second)
		return
	}
	//解析yaml文件
	err = yaml.Unmarshal(yamladmin, &admin)
	if err != nil {
		log.Println("解析文件失败,请检查配置文件是否正确")
		time.Sleep(1 * time.Second)
		return
	}
	//提示用户输入QQ号码
	var masterQQ int64
	fmt.Printf("请输入QQ号码: ")
	fmt.Scanln(&masterQQ)
	//判断用户输入的QQ号码是否已经存在
	for i, v := range admin.MasterQQ {
		if v == masterQQ {
			//删除元素
			admin.MasterQQ = append(admin.MasterQQ[:i], admin.MasterQQ[i+1:]...)
			//将结构体转换成yaml格式
			// //将结构体转换成yaml格式
			data, err := yaml.Marshal(&admin)
			if err != nil {
				log.Println("转换yaml格式失败")
				time.Sleep(1 * time.Second)
				return
			}
			//打印yaml格式（测试）
			// fmt.Println(string(data))
			//将yaml格式的数据写入到文件中
			if err := ioutil.WriteFile(admin_path, data, 0644); err != nil {
				log.Println("写入文件失败")
				time.Sleep(1 * time.Second)
				return
			}
			//绿色字体输出
			fmt.Println("\033[32m Success：配置文件已写入修改成功\033[0m")
			//查看文件路径
			fmt.Println("文件路径：", admin_path)
			time.Sleep(1 * time.Second)
			return
		}
		//不等于的情况
		//警告黄色字体输出
		if i == len(admin.MasterQQ)-1 {
			fmt.Println("\033[33m Warning：该QQ号码不存在\033[0m")
			time.Sleep(2 * time.Second)
		}
	}
}

// 查看管理员
func Show_manager(admin_path string) {

	var i int = 1
	//读取yaml文件，查看masterQQ序列
	//读取yaml文件
	var admin Admin
	yamladmin, err := ioutil.ReadFile(admin_path)
	if err != nil {
		log.Println("检查文件失败，请初始化云崽/喵崽")
		time.Sleep(1 * time.Second)
		return
	}
	//解析yaml文件
	err = yaml.Unmarshal(yamladmin, &admin)
	if err != nil {
		log.Println("解析文件失败,请检查配置文件是否正确")
		time.Sleep(1 * time.Second)
		return
	}
	//打印切片
	// fmt.Println(admin.MasterQQ)
	//优化打印格式
	for _, v := range admin.MasterQQ {
		fmt.Printf("管理员%d账户：%v\n", i, v)
		i = i + 1
	}
	//按任意键继续
	fmt.Println()
	fmt.Printf("按回车继续")
	fmt.Scanln()
}

// 清空屏幕
func ClearLog() {
	var cmd *exec.Cmd
	if runtime.GOOS == "windows" {
		cmd = exec.Command("cmd", "/c", "cls")
	} else {
		cmd = exec.Command("clear")
	}
	cmd.Stdout = os.Stdout
	cmd.Run()
}
func YunZai_status_menu(YunZaiPath, nodePath string) {
	for {
		ClearLog()
		var status_menu string = `
        =========后台启动/停止机器人=========
          1.启动机器人(pnpm start)
          2.停止机器人(pnpm stop)
          3.重启机器人(pnpm restart)
          0.返回上一级
        请输入菜单编号：`
		var Login_type int
		fmt.Printf("\033[34m")
		fmt.Printf(status_menu)
		_, err := fmt.Scanln(&Login_type)
		if err != nil {
			fmt.Println("输入错误，请重新输入!")
			time.Sleep(2 * time.Second)
			continue
		}
		switch Login_type {
		case 1:
			ClearLog()
			fmt.Println("您选择了 1. 启动机器人")
			if err := YunZai_start(YunZaiPath, nodePath, "start"); err != nil {
				log.Println("Node启动失败", err)
				time.Sleep(2 * time.Second)
			}
		case 2:
			ClearLog()
			fmt.Println("您选择了 2. 停止机器人")
			if err := YunZai_start(YunZaiPath, nodePath, "stop"); err != nil {
				log.Println("Node启动失败", err)
				time.Sleep(2 * time.Second)
			}
		case 3:
			ClearLog()
			fmt.Println("您选择了 3. 重启机器人")
			if err := YunZai_start(YunZaiPath, nodePath, "restart"); err != nil {
				log.Println("Node启动失败", err)
				time.Sleep(2 * time.Second)
			}
		case 0:
			ClearLog()
			fmt.Println("您选择了 0. 返回上一级")
			return
		default:
			fmt.Println("输入错误，请重新输入!")
			time.Sleep(2 * time.Second)
		}
	}
}

func YunZai_start(YunZaiPath, nodePath, order string) error {
	os.Setenv("NODE_HOME", nodePath)
	if err := os.Chdir(YunZaiPath); err != nil {
		return err
	}
	var cmd *exec.Cmd
	switch order {
	case "start":
		cmd = exec.Command("bash", "-c", "pnpm start")
	case "stop":
		cmd = exec.Command("bash", "-c", "pnpm stop")
	case "restart":
		cmd = exec.Command("bash", "-c", "pnpm restart")
	default:
		return fmt.Errorf("输入错误，请重新输入")
	}
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Start()
	cmd.Wait()
	//绿色字体输出
	fmt.Println("\033[32m Success：操作成功\033[0m")
	time.Sleep(2 * time.Second)
	return nil
}

// 5.查看当前配置
func Show_Conf(account_path, admin_path string) {
	//读取yaml文件1文件账户和密码
	//读取yaml文件2管理员列表

	type Config struct {
		QQ       string   `yaml:"qq"`
		Password string   `yaml:"pwd"`
		MasterQQ []string `yaml:"masterQQ"`
	}

	var config Config
	var admin Config

	yamlaccount, err := ioutil.ReadFile(account_path)
	if err != nil {
		log.Println("检查文件失败，请初始化云崽/喵崽")
		time.Sleep(1 * time.Second)
		return
	}
	//解析yaml文件
	err = yaml.Unmarshal(yamlaccount, &config)
	if err != nil {
		log.Println("解析文件失败,请检查配置文件是否正确")
		time.Sleep(1 * time.Second)
		return
	}
	//打印账户和密码
	//如果为空，就提示用户没有配置
	//绿色加粗字体输出
	fmt.Printf("\033[1;32m%s\033[0m\n", "机器人账户配置：")

	if config.QQ == "" {
		//红色字体输出
		fmt.Println("\033[31m Warning：当前机器人账户没有配置\033[0m")

	} else {
		fmt.Printf("账户：%v\n密码：%v\n", config.QQ, config.Password)
	}
	fmt.Println()
	yamladmin, err := ioutil.ReadFile(admin_path)
	if err != nil {
		log.Println("检查文件失败，请初始化云崽/喵崽")
		time.Sleep(1 * time.Second)
		return
	}
	//解析yaml文件
	err = yaml.Unmarshal(yamladmin, &admin)
	if err != nil {
		log.Println("解析文件失败,请检查配置文件是否正确")
		time.Sleep(1 * time.Second)
		return
	}
	//打印管理员列表
	fmt.Printf("\033[1;32m%s\033[0m\n", "管理员列表：")

	for i, v := range admin.MasterQQ {
		fmt.Printf("管理员%d：%v\n", i+1, v)
	}
	//空行站位
	fmt.Println()
	//按任意键继续
	fmt.Printf("按回车继续")
	fmt.Scanln()
}

// 7.查看当前日志
func YunZai_logs(YunZaiPath, nodePath string) error {
	//配置Node环境变量
	os.Setenv("NODE_HOME", nodePath)
	// 切换到指定目录
	if err := os.Chdir(YunZaiPath); err != nil {
		return err
	}

	// 启动应用程序
	cmd := exec.Command("bash", "-c", "pnpm run log")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Start(); err != nil {
		return err
	}
	if err := cmd.Wait(); err != nil {
		return err
	}
	return nil
}

// 修改QQ登录接口
func Set_LoginType(account_path string) {
	type Account struct {
		QQ       string `yaml:"qq"`
		Password string `yaml:"pwd"`
		Platform string `yaml:"platform"`
	}

	var account Account

	// 读取账户信息
	data, err := ioutil.ReadFile(account_path)
	if err != nil {
		log.Println("读取文件失败，请初始化云崽/喵崽")
		time.Sleep(2 * time.Second)
		return
	}
	if err := yaml.Unmarshal(data, &account); err != nil {
		log.Println("解析文件失败，请检查配置文件是否正确")
		time.Sleep(2 * time.Second)
		return
	}

	// 提示用户输入登录接口类型
	for {
		ClearLog()
		var choice int
		var status_menu string = `
        ==========修改QQ登录接口==========
          1：安卓手机
          2：aPad
          3：安卓手表
          4：MacOS
          5：iPad
          6：安卓8.8.88
          0.返回上一级
        请输入菜单编号：`
		fmt.Printf("\033[34m")
		fmt.Printf(status_menu)
		fmt.Scanln(&choice)
		switch choice {
		case 1:
			account.Platform = "1"
		case 2:
			account.Platform = "2"
		case 3:
			account.Platform = "3"
		case 4:
			account.Platform = "4"
		case 5:
			account.Platform = "5"
		case 6:
			account.Platform = "6"
		case 0:
			return
		default:
			fmt.Println("输入错误，请重新输入!")
			time.Sleep(2 * time.Second)
			continue
		}
		break
	}

	// 将更新后的账户信息转换成 YAML 格式
	data, err = yaml.Marshal(&account)
	if err != nil {
		log.Println("转换 YAML 格式失败")
		time.Sleep(2 * time.Second)
		return
	}

	// 将 YAML 格式的数据写入到文件中
	if err := ioutil.WriteFile(account_path, data, 0600); err != nil {
		log.Println("写入文件失败")
		time.Sleep(2 * time.Second)
		return
	}

	// 绿色字体输出
	fmt.Println("\033[32m Success：配置文件已写入修改成功\033[0m")
	time.Sleep(2 * time.Second)
}
