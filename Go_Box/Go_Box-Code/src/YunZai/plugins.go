package YunZai

import (
	"bufio"
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"time"
)

type Plugin struct {
	ID          string
	Name        string
	Description string
	Path        string
	git         string
	depend      string
}

func Plugins_main(YunZaiPath, Go_Box_path string) {
	// 指定要读取的插件信息文件路径
	//去云崽的目录
	filePath := Go_Box_path + "/plugins.txt"
	os.Chdir(YunZaiPath)
	//判断是否存在文件索引
	//判断文件时间是否超过1天
	//如果超过1天，更新插件索引
	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		//文件不存在，更新插件索引
		up_plugins_index(filePath)
	} else {
		//文件存在，判断时间是否超过1天
		fileInfo, _ := os.Stat(filePath)
		modTime := fileInfo.ModTime()
		//获取当前时间
		now := time.Now()
		//获取时间差
		diff := now.Sub(modTime)
		//如果时间差大于1小时，更新插件索引
		if diff.Hours() > 1 {
			up_plugins_index(filePath)
		}

	}
	// os.Chdir("C:/Users/lenovo/Desktop/YunZai")
	// 打开插件信息文件
	file, err := os.Open(filePath)
	if err != nil {
		// 处理文件打开错误
		panic(err)
	}
	defer file.Close()

	// 读取插件信息文件的每一行，并将其解析为插件结构体
	var plugins []Plugin
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		fields := strings.Split(line, "#")
		if len(fields) < 5 {
			// 处理不完整的插件信息
			continue
		}
		//拼接路径再赋值
		// fields[4] = "C:/Users/lenovo/Desktop/YunZai/plugins/" + fields[4]
		plugins = append(plugins, Plugin{ID: fields[1], Name: fields[2], Description: fields[3], Path: fields[4], git: fields[5]})
		//判断是否需要安装依赖
		if len(fields) == 7 {
			plugins[len(plugins)-1].depend = fields[6]
		}
	}

	// 按照每页 20 条的方式分页显示插件信息，直到用户输入 q 命令退出程序
	pageSize := 20
	page := 0
	for {
		start := page * pageSize
		end := start + pageSize

		if end > len(plugins) {
			end = len(plugins)
		}
		//清除颜色
		fmt.Print("\033[0m")
		fmt.Printf("第%d页:\n", page+1)
		for _, plugin := range plugins[start:end] {
			_, err := os.Stat(plugin.Path)
			if err == nil {
				//绿色显示已安装
				fmt.Printf("#%s\t%s\t%s\t\033[32m-----已安装\033[0m\n", plugin.ID, plugin.Name, plugin.Description)
				//打印路径
				// fmt.Printf("安装路径：%s\n", plugin.Path)
			} else {
				//红色显示未安装
				fmt.Printf("#%s\t%s\t%s\t\033[31m-----未安装\033[0m\n", plugin.ID, plugin.Name, plugin.Description)
			}
		}
		//打印提示
		fmt.Println()
		fmt.Print("按“q”退出、按“i”安装/升级、按“r”删除、或按其他键继续:")
		reader := bufio.NewReader(os.Stdin)
		input, _ := reader.ReadString('\n')
		if strings.TrimSpace(input) == "q" {
			break
		}
		//如果输入i，安装/升级
		if strings.TrimSpace(input) == "i" {
			fmt.Println("安装/升级")
			installPlugin(plugins, YunZaiPath)
		}
		//如果输入r，删除
		if strings.TrimSpace(input) == "r" {
			fmt.Println("删除")
			uninstallPlugin(plugins)
		}
		page++
		//如果到尾页，返回首页
		if page*pageSize >= len(plugins) {
			page = 0
		}
		//清空屏幕
		// fmt.Print("\033[2J")
	}

	if err := scanner.Err(); err != nil {
		// 处理文件读取错误
		panic(err)
	}
}
func installPlugin(plugins []Plugin, YunZaiPath string) {
	wd, _ := os.Getwd()
	fmt.Println("当前工作目录：", wd)
	fmt.Print("请输入插件ID：")
	//输入小于10的数字，自动补0
	var id string
	fmt.Scanln(&id)
	//判断用户是否输入1-9不带0的数字
	if len(id) == 1 {
		id = "0" + id
	}

	//根据插件ID查找插件
	var plugin Plugin
	for _, p := range plugins {
		if p.ID == id {
			plugin = p
			break
		}
	}

	//如果插件已安装，打印错误信息
	_, err := os.Stat(plugin.Path)
	if err == nil {
		fmt.Println("插件已安装")
		//提示是否升级
		fmt.Print("是否升级该插件？（y/n）")
		var confirm string
		fmt.Scanln(&confirm)
		//如果用户不确认升级，直接退出
		if confirm != "y" {
			return
		}
		//执行升级命令
		fmt.Println("执行升级命令...")
		//去插件目录
		os.Chdir(plugin.Path)
		//执行git pull命令
		cmd := exec.Command("git", "pull")
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		err = cmd.Start()
		if err != nil {
			fmt.Println("升级失败")
		}
		//等待升级完成
		cmd.Wait()
		//返回插件目录
		//打印升级成功信息
		fmt.Println("升级成功")
		os.Chdir(wd)
		//返回插件目录
		return

	}
	//打印插件信息
	//判断索引是否越界
	if plugin.ID == "" {
		fmt.Println("插件不存在")
		return
	}
	fmt.Printf("插件ID：%s\n插件名称：%s\n插件描述：%s\n", plugin.ID, plugin.Name, plugin.Description)
	//读取用户输入的确认信息
	fmt.Print("是否安装该插件？（y/n）")
	var confirm string
	fmt.Scanln(&confirm)
	//如果用户不确认安装，直接退出
	if confirm != "y" {
		return
	}
	//执行安装命令
	fmt.Println("执行安装命令...")
	//去插件目录
	//读取插件的git地址，执行git clone命令
	//显示获取到的git地址
	//打印安装地址
	fmt.Println("安装命令", plugin.git)
	fmt.Println("依赖命令：", plugin.depend)
	//获取插件索引判断是否有依赖

	fmt.Println("安装地址：", plugin.Path)
	//渠道插件目录

	if err := os.MkdirAll("plugins", 0755); err != nil {
		// 处理错误
	}
	//git clone 到插件目录
	cmd := exec.Command("git", "clone", plugin.git, plugin.Path)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Start()
	if err != nil {
		fmt.Println("安装失败")

	}
	//等待安装完成
	cmd.Wait()
	fmt.Println(plugin.depend)
	if plugin.depend != "" {
		//去云崽目录
		os.Chdir(YunZaiPath)
		fmt.Println("安装依赖...")
		//运行依赖命令
		cmd := exec.Command("bash", "-c", plugin.depend)
		yes := []byte("y\n")
		cmd.Stdin = ioutil.NopCloser(bytes.NewReader(yes))
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		err := cmd.Run()
		if err != nil {
			// 处理错误
			fmt.Println("安装失败")
		}
		//等待安装完成
		cmd.Wait()
		//返回插件目录

	}
	//打印安装成功信息
	fmt.Println("安装成功")
}
func uninstallPlugin(plugins []Plugin) {
	//云崽目录
	//输入插件ID
	fmt.Print("请输入插件ID：")
	//输入小于10的数字，自动补0
	var id string
	fmt.Scanln(&id)
	//判断用户是否输入1-9不带0的数字
	if len(id) == 1 {
		id = "0" + id
	}
	//根据插件ID查找插件
	var plugin Plugin
	for _, p := range plugins {
		if p.ID == id {
			plugin = p
			break
		}
	}
	//如果插件未安装，打印错误信息
	_, err := os.Stat(plugin.Path)
	if err != nil {
		fmt.Println("插件未安装")
		return
	}
	if plugin.ID == "" {
		fmt.Println("插件不存在")
		return
	}
	fmt.Printf("插件ID：%s\n插件名称：%s\n插件描述：%s\n", plugin.ID, plugin.Name, plugin.Description)
	//读取用户输入的确认信息
	fmt.Print("是否卸载该插件？（y/n）")
	var confirm string
	fmt.Scanln(&confirm)
	if confirm != "y" {
		return
	}
	fmt.Println("插件相对路径：", plugin.Path)
	//执行卸载命令
	err = os.RemoveAll(plugin.Path)
	if err != nil {
		fmt.Println("卸载失败")
		return
	}
	_, err = os.Stat(plugin.Path)
	if err != nil {
		fmt.Println("卸载成功")
		return
	} else {
		fmt.Println("卸载失败")
		return
	}
}

// 更新插件索引
func up_plugins_index(filePath string) {
	url := "https://gitee.com/Liplay-1/YBOX/raw/master/plugins.txt"
	resp, err := http.Get(url)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	err = ioutil.WriteFile(filePath, body, 0644)
	if err != nil {
		panic(err)
	}
}
