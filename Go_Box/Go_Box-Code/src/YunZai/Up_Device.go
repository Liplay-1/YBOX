package YunZai

/*
feature: 重新生成设备文件
*/
import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"math/rand"
	"net"
	"os"
	"time"
)

type DeviceConfig struct {
	Begin       string `json:"--begin--"`
	Product     string `json:"product"`
	Device      string `json:"device"`
	Board       string `json:"board"`
	Brand       string `json:"brand"`
	Model       string `json:"model"`
	WifiSSID    string `json:"wifi_ssid"`
	Bootloader  string `json:"bootloader"`
	AndroidID   string `json:"android_id"`
	BootID      string `json:"boot_id"`
	ProcVersion string `json:"proc_version"`
	MacAddress  string `json:"mac_address"`
	IPAddress   string `json:"ip_address"`
	IMEI        string `json:"imei"`
	Incremental string `json:"incremental"`
	End         string `json:"--end--"`
}

// 测试main
func Up_Device(device_path, YunZaiPath string) {
	var qq string
	var filename string = "device.json"
	// 读取用户输入的文件名
	// 保存路径
	// 拼接路径
	fmt.Print("请输入机器人QQ号：")
	fmt.Scanln(&qq)
	//判断文件夹是否存在
	qq = device_path + "/" + qq
	qq2 := YunZaiPath + "/data/" + "device.json"
	if _, err := os.Stat(qq); os.IsNotExist(err) {
		//文件不存在，创建文件夹
		err := os.MkdirAll(qq, 0777)
		if err != nil {
			fmt.Println("创建文件夹失败")
			return
		}
	}

	// 把文件放在文件夹下
	filename = qq + "/" + filename
	fmt.Println("保存路径为：", filename)
	// 随机生成设备配置
	config := generateDeviceConfig("Samsung")

	// 将设备配置序列化为 JSON 字符串
	data, err := json.MarshalIndent(config, "", "  ")
	if err != nil {
		fmt.Println("设备配置无法进行编组:", err)
		return
	}

	// 将 JSON 字符串写入文件
	file1, err := os.Create(qq2)
	if err != nil {
		fmt.Println("创建文件失败:", err)
		return
	}
	file, err := os.Create(filename)
	if err != nil {
		fmt.Println("创建文件失败:", err)
		return
	}
	defer file.Close()
	defer file1.Close()
	_, err = file.Write(data)
	if err != nil {
		fmt.Println("数据写入文件时发生了错误:", err)
		return
	}
	_, err = file1.Write(data)
	if err != nil {
		fmt.Println("数据写入文件时发生了错误:", err)
		return
	}
	fmt.Println("\033[32m 设备文件生成成功，请重新登录机器人\033[32m")
	time.Sleep(2 * time.Second)
}

func generateDeviceConfig(brand string) DeviceConfig {
	// 初始化随机数生成器
	rand.Seed(time.Now().UnixNano())

	// 随机生成设备配置
	config := DeviceConfig{
		Begin:       "该设备为随机生成，丢失后不能得到原先配置",
		Product:     "ILPP-281DF",
		Device:      randomHexString(5),
		Board:       randomHexString(5),
		Brand:       brand,
		Model:       "ILPP " + randomHexString(4),
		WifiSSID:    randomSSID(),
		Bootloader:  "U-boot",
		AndroidID:   "IL." + randomHexString(7) + ".9912",
		BootID:      randomUUID(),
		ProcVersion: "Linux version " + randomHexString(8) + "-android12-" + randomHexString(8),
		MacAddress:  randomMACAddress(),
		IPAddress:   randomIPAddress(),
		IMEI:        randomIMEI(),
		Incremental: randomHexString(10),
		End:         "修改后可能需要重新验证设备。",
	}

	return config
}

func randomHexString(n int) string {
	bytes := make([]byte, n/2)
	rand.Read(bytes)
	return fmt.Sprintf("%X", bytes)
}

func randomUUID() string {
	uuid := make([]byte, 16)
	rand.Read(uuid)
	uuid[8] = uuid[8]&^0xc0 | 0x80
	uuid[6] = uuid[6]&^0xf0 | 0x40
	return fmt.Sprintf("%X-%X-%X-%X-%X", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:])
}

func randomSSID() string {
	letters := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	name := make([]byte, 8)
	for i := range name {
		name[i] = letters[rand.Intn(len(letters))]
	}
	return "HUAWEI-" + string(name)
}

func randomMACAddress() string {
	addr := make(net.HardwareAddr, 6)
	rand.Read(addr)
	return addr.String()
}

func randomIPAddress() string {
	ip := make(net.IP, 4)
	rand.Read(ip)
	return ip.String()
}

func randomIMEI() string {
	imei := make([]byte, 8)
	rand.Read(imei)
	hexStr := hex.EncodeToString(imei)
	return fmt.Sprintf("%s%04d", hexStr, rand.Intn(10000))
}
