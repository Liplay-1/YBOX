package Scripts

import (
	"fmt"
	"os"
	"os/exec"
	"time"
)

// 下载repair脚本，运行
func Repair_main(repair_path, YunZaiPath, Go_Box_path string) {
	//判断是否存在repair.py,如果存在超过1小时，重新下载
	//判断是否存在repair.py
	repair_path = repair_path + "/scripts/repair.sh"
	scripts := Go_Box_path + "/scripts"
	if _, err := os.Stat(scripts); os.IsNotExist(err) {
		os.Mkdir(scripts, 0755)
	}
	if _, err := os.Stat(repair_path); os.IsNotExist(err) {
		//绿色输出
		fmt.Println("\033[32m", "文件不存在,获取中...", "\033[0m")
		//文件不存在，下载repair.py
		Url_update("https://gitee.com/Liplay-1/YBOX/raw/master/Go_Box/Scripts/repair.sh", repair_path)
		//判断是否存在repair.py
		if _, err := os.Stat(repair_path); os.IsNotExist(err) {
			//红色输出
			fmt.Println("\033[31m", "文件下载失败，请检查网络...", "\033[0m")
			//运行完毕，返回
			return
		} else {
			fileInfo, _ := os.Stat(repair_path)
			modTime := fileInfo.ModTime()
			fmt.Println("\033[32m", "文件下载成功，更新时间为：", modTime.Format("2006-01-02 15:04:05"), "\033[0m")
		}

		//睡眠2秒
		time.Sleep(2 * time.Second)

	} else {
		fileInfo, _ := os.Stat(repair_path)
		modTime := fileInfo.ModTime()
		// //获取当前时间
		now := time.Now()
		// //获取时间差
		diff := now.Sub(modTime)
		// //如果时间差大于12小时，重新下载
		if diff.Hours() > 12 {
			Url_update("https://gitee.com/Liplay-1/YBOX/raw/master/Go_Box/Scripts/repair.sh", repair_path)
		}
	}
	//运行脚本，并且可以交互
	cmd := exec.Command("bash", repair_path, YunZaiPath)
	cmd.Stdout = os.Stdout
	cmd.Stdin = os.Stdin
	cmd.Stderr = os.Stderr
	cmd.Run()
	//运行完毕，返回
	return
}
