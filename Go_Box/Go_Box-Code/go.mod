module Go_Box

go 1.20

require gopkg.in/yaml.v2 v2.4.0

require (
	github.com/fatih/color v1.15.0 // indirect
	golang.org/x/sync v0.2.0 // indirect
)
