package main

import (
	"Go_Box/src/Scripts"
	"Go_Box/src/YunZai"
	"fmt"
	"log"
	"os"
	"os/exec"
	"runtime"
	"time"
)

var (
	Go_Box_path string = "/root/Go_Box"     //Go_Box路径
	YunZaiPath  string = "/root/Yunzai-Bot" //云崽路径
	//node path
	nodePath     string = "/usr/local/bin/node"                    //node路径
	account_path string = "/root/Yunzai-Bot/config/config/qq.yaml" //账号配置文件路径
	//管理员账号配置文件路径
	admin_path string = "/root/Yunzai-Bot/config/config/other.yaml" //管理员账号配置文件路径
	//设备文件路径
	device_path string = "/root/Yunzai-Bot/data/icqq" //icqq存储路径

)

func YunZai_menu() {
	// 要判断是否存在YunZai，不存在则退出
	//临时注释
	// if _, err := os.Stat(YunZaiPath); os.IsNotExist(err) {
	// 	fmt.Println("\033[31m")
	// 	fmt.Println("路径不存在，请检查是否安装喵崽/云崽,按回车键退出")
	// 	bufio.NewReader(os.Stdin).ReadBytes('\n')
	// 	fmt.Println("\033[0m")
	// 	return
	// }

	var YunZai_menu = `
	==========云崽BOX工具箱==========
	  1.初始化/掉线登录
	  2.修改机器人账户
	  3.修改/添加管理员账号
	  4.后台启动/停止机器人
	  5.查看当前配置
	  6.重新生成设备文件
	  7.查看当前日志
	  8.修改QQ登录接口
	  9.重新安装云崽
	  10.Python管理器
	  11.云崽插件安装/卸载（欢迎提交）
	  12.常见问题修复
	  -1. 检查更新
	  0.退出
	请输入菜单编号：`
	var choice int
	for {
		clearLog()
		fmt.Printf("\033[34m")
		fmt.Printf(YunZai_menu)
		_, err := fmt.Scanln(&choice)
		if err != nil {
			fmt.Println("输入错误，请重新输入")
			continue
		}
		switch choice {
		case -1:
			fmt.Println("您选择了 -1. 更新Box")
			Scripts.Update_main(Go_Box_path)
		case 0:
			fmt.Println("您选择了 0. 退出")
			fmt.Println("\033[0m")
			os.Exit(0)
		case 1:
			fmt.Println("您选择了 1. 初始化\\掉线登录")
			if err := YunZai.YunZai_init(YunZaiPath, nodePath); err != nil {
				log.Println("Node启动失败", err)
				return
			}
		case 2:
			clearLog()
			fmt.Println("您选择了 2. 修改机器人账户")
			YunZai.Set_Account(account_path)
		case 3:
			clearLog()
			YunZai.Set_YunZaiAdmin(admin_path)
		case 4:
			YunZai.YunZai_status_menu(YunZaiPath, nodePath)
		case 5:
			clearLog()
			fmt.Println("您选择了 5. 查看当前配置")
			YunZai.Show_Conf(account_path, admin_path)
		case 6:
			clearLog()
			fmt.Println("您选择了 6. 重新生成设备文件")
			YunZai.Up_Device(device_path, YunZaiPath)
		case 7:
			clearLog()
			fmt.Println("您选择了 7. 查看当前日志")
			if err := YunZai.YunZai_logs(YunZaiPath, nodePath); err != nil {
				log.Println("获取失败", err)
				return
			}
		case 8:
			clearLog()
			YunZai.Set_LoginType(account_path)
		case 9:
			clearLog()
			fmt.Println("您选择了 9. 重新安装云崽")
			Scripts.Install_main(Go_Box_path)
		case 10:
			clearLog()
			fmt.Println("您选择了 10. Python管理器")
			Scripts.Python_main(Go_Box_path)
		case 11:
			clearLog()
			fmt.Println("您选择了 11. 云崽插件安装/卸载")
			YunZai.Plugins_main(YunZaiPath, Go_Box_path)
		case 12:
			clearLog()
			fmt.Println("您选择了 12. 常见问题修复")
			Scripts.Repair_main(Go_Box_path, YunZaiPath, Go_Box_path)
		default:
			clearLog()
			fmt.Println("无效的选项，请重新输入")
			time.Sleep(1 * time.Second)
			clearLog()
		}
	}
}

// 清空屏幕
func clearLog() {
	var cmd *exec.Cmd
	if runtime.GOOS == "windows" {
		cmd = exec.Command("cmd", "/c", "cls")
	} else {
		cmd = exec.Command("clear")
	}
	cmd.Stdout = os.Stdout
	cmd.Run()
}

func main() {
	//测试变量有没有取到值
	// fmt.Println("YunZaiPath: ", YunZaiPath)
	// fmt.Println("nodePath ", nodePath)
	clearLog()
	YunZai_menu()
}
