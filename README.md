<div align="center">
<h2>云崽一键安装脚本</h2>

[![访问量](https://profile-counter.glitch.me/Liplay-1/count.svg)](https://gitee.com/Liplay-1/YBOX/)


<a href='https://gitee.com/Liplay-1/YBOX/stargazers'><img src='https://gitee.com/Liplay-1/YBOX/badge/star.svg?theme=dark' alt='star'></img></a>
<a href='https://gitee.com/Liplay-1/YBOX/members'><img src='https://gitee.com/Liplay-1/YBOX/badge/fork.svg?theme=white' alt='fork'></img></a>

</div>


#### 使用教程

#####  简介


- [视频教程](https://www.bilibili.com/video/BV1DY411z7Ry/?share_source=copy_web&vd_source=a461791735fdb518bb5f9cf3b7718ef6)
云崽一键安装脚本，花了两周重新编写，现适用于Centos7、Centos8、Centos_stream8、UBuntu20.04、UBuntu22.04、UBuntu18.04、Debian11（大部分我测试过时没什么问题的）直接运行可以了，推荐使用UBuntu20.04去部署，功能比较全。插件索引文件是，plugins.txt，其他由你们来测试了。
- [Yunzai-Bot 相关内容索引](https://gitee.com/yhArcadia/Yunzai-Bot-plugins-index)
- [Yunzai-Bot 帮助文档](https://docs.yunzai.org/)
##### 部署脚本
```
sudo -i
curl -sL https://gitee.com/Liplay-1/YBOX_2/raw/master/main.sh > /root/main.sh
bash /root/main.sh
```
##### 安卓部署
```
待完善
```
##### Win系统
```
待完善
```
#### 欢迎加入交流群
![输入图片说明](imgimage.png)
#### 功能介绍

- [x] 一键安装部署环境
- [x] 启动/重启/停止
- [x] 插件的安装/更新/卸载
- [x] Python管理
- [x] 账户修改
- [x] 管理员/添加/删除/修改

#### 云崽根目录说明

 | 目录                       | 说明              |
  |--------------------------|-----------------|
  | config\config\qq.yaml    | 修改或更换机器人账号-登录方式 |
  | config\config\other.yaml | 修改或添加主人QQ       |
  | data\MysCookie           | 存放 cookie 的位置   |
  | plugins\example          | 存放 js 插件的位置     |
  | Yunzai-Bot\plugins       | 存放大型插件的位置，如喵喵插件 |
  | Yunzai-Bot\data\face     | 存放添加的图片词条 |
  | Yunzai-Bot\data\textJson | 存放添加的文字词条 |

#### 其他
- 脚本开源，尊重别人劳动成果,禁止倒卖
- 仅供交流学习使用，严禁用于任何商业用途和非法行为





